﻿using knopka.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace knopka.Services
{
    public interface IDataService
    {
        Task<bool> GetData();

        Task<string> Get();

       IEnumerable<Album> Parse(string str);

        Task<bool> Save(IEnumerable<Album> albums);
    }
}
