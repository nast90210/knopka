﻿using knopka.Data;
using knopka.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace knopka.Repository
{
    public class AlbumRepository : IAlbumRepository
    {
        private readonly ApplicationDbContext _context;

        public AlbumRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> Add(Album album)
        {
            _context.Albums.Add(album);

            return await SaveChangesAsync();
        }

        public async Task<Album> Get(int id)
        {
            return await _context.Albums
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Album>> GetAll() =>
            await _context.Albums.ToListAsync();

        public async Task<bool> Remove(int id)
        {
            var album = await Get(id);

            if (album != null)
            {
                _context.Albums.Remove(album);
            }

            return await SaveChangesAsync();
        }

        public async Task<bool> SaveChangesAsync() => 
            await _context.SaveChangesAsync() > 0;
    }
}
