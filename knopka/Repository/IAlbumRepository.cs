﻿using knopka.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace knopka.Repository
{
    public interface IAlbumRepository
    {
        Task<IEnumerable<Album>> GetAll();

        Task<Album> Get(int id);

        Task<bool> Add(Album album);

        Task<bool> Remove(int id);
    }
}
