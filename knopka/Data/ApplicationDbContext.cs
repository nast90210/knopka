﻿using knopka.Models;
using Microsoft.EntityFrameworkCore;

namespace knopka.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        public DbSet<Album> Albums { get; set; }
    }
}
