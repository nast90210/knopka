﻿using Microsoft.Extensions.DependencyInjection;

namespace knopka.Data
{
    public class DbInitializer
    {
        public static void Initialize(IServiceScope scope) {
            var services = scope.ServiceProvider;
            var ctx = services.GetRequiredService<ApplicationDbContext>();

            ctx.Database.EnsureCreated();
        }
    }
}
