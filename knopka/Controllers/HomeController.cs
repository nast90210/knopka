﻿using knopka.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace knopka.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDataService _dataService;

        public HomeController(IDataService dataService)
        {
            _dataService = dataService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetData()
        {
            var result = await _dataService.GetData();

            return Ok(result);
        }
    }
}
